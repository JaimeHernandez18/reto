from modelchoices import Choices


class CustomerChoices(Choices):
    uno = ('1', 'parser1')
    dos = ('2', 'parser2')
    tres = ('3', 'parser3')


class UploadChoices(Choices):
    by_email = ('byEmail', 'Email')
    upload_by_user = ('upload-by-user', 'upload-by-user')


class SendChoices(Choices):
    true = ('True', 'true')
    false = ('False', 'false')
