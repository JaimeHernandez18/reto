from .models import Customers, FileParser
from django import forms


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customers

        fields = [
            'name',
            'parser',
        ]

        labels = {
            'name': 'Name',
            'parser': 'Parser',
        }

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'parser': forms.Select(attrs={'class': 'form-control'}),
        }


class FileParserForm(forms.ModelForm):
    class Meta:
        model = FileParser
        fields = [
            'customer',
            'emailID',
            'emailDate',
            'options',
            'send',
            'file',
            'created_at'
        ]

        labels = {
            'customer': 'Customer',
            'emailID': 'Email id',
            'emailDate': 'Email Date',
            'options': 'Option',
            'send': 'Send',
            'file': 'File',
            'created_at': 'Create at'
        }

        widgets = {
            'customer': forms.Select(attrs={'class': 'form-control'}),
            'emailID': forms.TextInput(attrs={'class': 'form-control', 'readonly': 'true'}),
            'emailDate': forms.DateTimeInput(attrs={'class': 'form-control', 'type': 'date'}),
            'options': forms.Select(attrs={'class': 'form-control'}),
            'send': forms.Select(attrs={'class': 'form-control'}),
            'file': forms.ClearableFileInput(attrs={'multiple': True}),
            'created_at': forms.DateTimeInput(attrs={'class': 'form-control', 'type': 'date'})
        }
