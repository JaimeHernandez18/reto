import uuid
from django.db import models
from .choices import CustomerChoices, UploadChoices, SendChoices
from .utils.file_validation import validation


class Customers(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45)
    parser = models.CharField(max_length=25, choices=CustomerChoices.CHOICES, null=False)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class FileParser(models.Model):
    id = models.AutoField(primary_key=True)
    customer = models.ForeignKey(Customers, null=False, on_delete=models.CASCADE)
    emailID = models.UUIDField(default=uuid.uuid4)
    emailDate = models.DateTimeField()
    parse = models.CharField(max_length=25, null=False)
    options = models.CharField(max_length=25, choices=UploadChoices.CHOICES, null=False)
    send = models.CharField(max_length=10, choices=SendChoices.CHOICES, null=False)
    file = models.FileField(null=False, validators=[validation])
    created_at = models.DateTimeField()

    def __str__(self):
        return f"Customer: {self.customer.name} - EmailID: {self.emailID}"
