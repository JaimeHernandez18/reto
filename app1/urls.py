from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('create_customer/', views.CreateCustomer.as_view(), name='create_customer'),
    path('add_file/', views.AddFile.as_view(), name='add_file'),
    path('filter', views.FilterView.as_view(), name='filter'),
    ####
    path('success_customer/', views.success_customer, name='success_customer'),
    path('success_file/', views.success_file, name='success_file'),
    path('results/', views.results, name='results')
]
