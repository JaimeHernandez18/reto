from django.core.exceptions import ValidationError


def validation(file):
    """
    This function is to validate the extension and size of the file

    :param file:
    :return: ValidationError or Boolean
    """

    if int(file.size) < 100000000:
        return True
    raise ValidationError('The file must weigh 100 MB maximum')
