# Create your views here.
import datetime
import pandas
from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import CreateView, ListView
from .models import Customers, FileParser
from .forms import CustomerForm, FileParserForm
import uuid


def index(request):
    return render(request, 'index.html')


def success_customer(request):
    return render(request, 'success/success_customer.html')


def success_file(request):
    return render(request, 'success/success_file.html')


def results(request):
    return render(request, 'success/results.html')


class CreateCustomer(CreateView):
    model = Customers
    template_name = 'customer.html'
    form_class = CustomerForm
    success_url = reverse_lazy('success_customer')


class AddFile(CreateView):
    model = FileParser
    template_name = 'addfile.html'
    form_class = FileParserForm
    success_url = reverse_lazy('success_file')


class FilterView(View):

    def __init__(self):
        self.template_name = '../templates/filter.html'
        self.customers = Customers.objects.all()

    def get(self, request):
        return render(request, self.template_name, {'customers': self.customers})

    def post(self, request):
        filter = str(self.request.POST['filter_by'])
        if filter == 'Email_date':
            if FileParser.objects.filter(emailDate__gte=request.POST['date_initial'],
                                         emailDate__lte=request.POST['finish_date'], customer=request.POST['customer']):
                type = []
                create_date = []
                customer = []
                sent = []
                list = []
                info = FileParser.objects.filter(customer=self.request.POST['customer'])
                for inf in info:
                    type.append(str(inf.file))
                    create_date.append(str(inf.created_at))
                    customer.append(inf.customer.name)
                    sent.append(inf.send)
                for i in range(0, len(info)):
                    list.append([type[i], create_date[i], customer[i], sent[i]])
                df = pandas.DataFrame({'type': type, 'create_at ': create_date, 'customer': customer, 'sended': sent})
                link = f'media/informe-{str(uuid.uuid4().hex)}.xlsx'
                writer = pandas.ExcelWriter(link, engine='xlsxwriter')
                df.to_excel(writer, sheet_name='Sheet1')
                writer.save()
                context = {'list': list, 'link': link}
                return render(request, "success/results.html", context)
            else:
                return redirect('index')

        else:
            if FileParser.objects.filter(created_at__gte=request.POST['date_initial'],
                                         created_at__lte=request.POST['finish_date'],
                                         customer=request.POST['customer']):
                type = []
                create_date = []
                customer = []
                sent = []
                list = []
                info = FileParser.objects.filter(id=self.request.POST['customer'])
                for inf in info:
                    type.append(str(inf.file))
                    create_date.append(str(inf.created_at))
                    customer.append(inf.customer.name)
                    sent.append(inf.send)
                for i in range(0, len(info)):
                    list.append([type[i], create_date[i], customer[i], sent[i]])
                df = pandas.DataFrame({'type': type, 'create_at ': create_date, 'customer': customer, 'sended': sent})
                link = f'media/informe{datetime.datetime.now()}.xlsx'
                writer = pandas.ExcelWriter(link, engine='xlsxwriter')
                df.to_excel(writer, sheet_name='Sheet1')
                writer.save()
                context = {'list': list, 'link': link}
                return render(request, "success/results.html", context)
            else:
                return redirect('index')
