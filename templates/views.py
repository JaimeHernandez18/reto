from django.shortcuts import render
from apps.exporterimporter_app.models import FileParser
from .forms import *

# Create your views here.
from django.views.generic import CreateView


def index(request):
    return render(request, 'index.html')


# class Exporter(CreateView):
#     model = FileParser
#     template_name = 'exporter.html'
#     form_class = FileParserForm
